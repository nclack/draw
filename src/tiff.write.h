#pragma once
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void write_tiff_u8 (const char* filename,size_t ndim,const size_t* shape,const uint8_t  *data);
void write_tiff_i16(const char* filename,size_t ndim,const size_t* shape,const int16_t  *data);
void write_tiff_u16(const char* filename,size_t ndim,const size_t* shape,const uint16_t *data);
void write_tiff_f32(const char* filename,size_t ndim,const size_t* shape,const float    *data);

#ifdef __cplusplus
}
#endif